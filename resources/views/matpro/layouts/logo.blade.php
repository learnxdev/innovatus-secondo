<!-- ============================================================== -->
<!-- Logo -->
<!-- ============================================================== -->
<div class="navbar-header">
    <a class="navbar-brand" href="/">
        <!-- Logo icon -->
    <b>
        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
        <!-- Dark Logo icon
        <img src="../assets/images/logo-icon.png" alt="homepage" class="dark-logo" />-->
        <!-- Light Logo icon
        <img src="../assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />-->
    </b>
    <!--End Logo icon -->
    <!-- Logo text -->
    <span>
     <!-- dark Logo text
    <img src="../assets/images/logo-text.png" alt="homepage" class="dark-logo" />-->
     <!-- Light Logo text
    <img src="../assets/images/logo-light-text.png" class="light-logo" alt="homepage" />--></span>
    <span class="text-white"><b>Taco Tuesday</b></span> </a>
</div>
<!-- ============================================================== -->
<!-- End Logo -->
<!-- ============================================================== -->
