@extends('matpro.layouts.landing')
@section('scripts')
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '{{ env('FB_APPID') }}',
      xfbml      : true,
      version    : 'v2.10'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
@endsection
@section('content')
<div
  class="fb-like"
  data-share="true"
  data-width="450"
  data-show-faces="true">
</div>
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row m-t-40">
    <div class="col-md-12">
        <h4 class="card-title">What's cooking... </h4>
        <h6 class="card-subtitle m-b-20 text-muted">Place your order now</h6> </div>
</div>
<div class="card-columns el-element-overlay">
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="../assets/images/gallery/tacos-1.jpg"> <img src="../assets/images/gallery/tacos-1.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">A Number 1</h3> <small>Taco dinner for 1  </small>
                <br/> </div>
        </div>
    </div>
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="../assets/images/gallery/tacos-2.jpg"> <img src="../assets/images/gallery/tacos-2.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">A Number 2</h3> <small>Taco dinner for 2  </small>
                <br/> </div>
        </div>
    </div>
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="../assets/images/gallery/tacos-3.jpg"> <img src="../assets/images/gallery/tacos-3.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">A Number 3</h3> <small>Taco dinner for 3  </small>
                <br/> </div>
        </div>
    </div>
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="../assets/images/gallery/tacos-4.jpg"> <img src="../assets/images/gallery/tacos-4.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">A Number 4</h3> <small>Taco dinner for 4  </small>
                <br/> </div>
        </div>
    </div>
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="../assets/images/gallery/tacos-5.jpg"> <img src="../assets/images/gallery/tacos-5.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">A Number 5</h3> <small>Taco dinner for 5  </small>
                <br/> </div>
        </div>
    </div>
    <div class="card">
        <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
                <a class="image-popup-vertical-fit" href="../assets/images/gallery/tacos-6.jpg"> <img src="../assets/images/gallery/tacos-6.jpg" alt="user" /> </a>
            </div>
            <div class="el-card-content">
                <h3 class="box-title">A Number 6</h3> <small>Taco dinner for 6  </small>
                <br/> </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection
