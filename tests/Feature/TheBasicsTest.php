<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TheBasicsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Then one where a visitor visits the landing page.
     */
    public function testVisitorVisits()
    {
        $this->get('/')
            ->assertSee('TacoTuesday')
            ->assertSee('login');
    }

    /**
     * The one where a visitor visits the registration page.
     */
    public function testVisitorVisitsRegistration()
    {
        $this->get('/register')
            ->assertStatus(200);
    }

    /**
     * The one where a visitor visits the login page.
     */
    public function testVisitorVisitsLogin()
    {
        $this->get('/login')
            ->assertStatus(200);
    }

    /**
     * The one where a user logs in.
     */
    public function testUserLogsIn()
    {
        // Given I am signed in
        $this->signIn();
        $this->assertTrue(Auth::check());
    }

    /**
     * The one where a user logs out.
     */
    public function testUserLogsOut()
    {
        // Given I am signed in
        $this->signIn();
        $this->assertTrue(Auth::check());
        $this->get('/logout');
        $this->assertFalse(Auth::check());
        $this->withExceptionHandling();
        $this->get('home')->assertRedirect('/login');
    }
}
