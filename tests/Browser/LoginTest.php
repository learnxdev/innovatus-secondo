<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        Parent::setUp();
        $this->user = factory(User::class)->make(['email' => 'coach@voicebitz.com']);
        $this->user->save();
    }

    /**
     * The one where a user logs in.
     */
    public function testLogin()
    {
        /* $user = create(
             'App\User',
             ['email' => 'coach@voicebitz.com', 'password' => bcrypt('secret')]
         );*/

        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('email', $this->user->email)
                    ->type('password', 'secret')
                    ->press('LOGIN')
                    ->assertPathIs('/home');
        });
    }
}
